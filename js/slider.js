let position = 0;
let sildeCount = document.getElementsByClassName('slide').length - 1;
function goLeft() {
    if(position == sildeCount*100) {
        position = 0;
        document.getElementById('main-slide').style.opacity = 0.4;
    }
    else (position+=100);
    document.getElementById('main-slide').style.left = `-${position}vw`;
    setTimeout(()=>{ document.getElementById('main-slide').style.opacity = 1},500);
}

function goRight() {
    (position == 0) ? (document.getElementById('main-slide').style.animationName = 'moveLeft') : (position-=100);
    document.getElementById('main-slide').style.left = `-${position}vw`;
    setTimeout(()=>{ document.getElementById('main-slide').style.animationName = 'none'},500);
}