$(document).keydown(function(event) {
    if (event.ctrlKey && event.keyCode === 13) {
        // submit the form
        var str = window.getSelection();
        var cont = window.getSelection();
        var text = cont.baseNode.data;
        var tb_select = Math.min(cont.baseOffset,cont.extentOffset);
        var te_select = Math.max(cont.baseOffset,cont.extentOffset);
        console.log(tb_select + ' ' +te_select );
        var t_length =  te_select - tb_select;

        var sel_text = text.substr(tb_select,t_length);

        if (sel_text.length > 0 && sel_text.length < 501) {
            var a = $('#orphos');

            a.find('#error-text').html(text);
            a.find('#error-word').html(sel_text);

            a.find('[name="Orphos[error_text]"]').val(text);
            a.find('[name="Orphos[error_word]"]').val(sel_text);
            a.find('[name="Orphos[error_url]"]').val( window.location.href );
            $('#orphos').modal();
        }
        else if(sel_text.length ==0){
            alert(orhpos_lang['er_small_text']);
        }
        else {
            alert(orhpos_lang['er_big_text']);
        }
    }
});

$( document ).on('beforeSubmit','#orphos-form',function (data) {
    data.preventDefault();
    data.stopPropagation();


    var yiiform = $(this);

    $.ajax({
            type: yiiform.attr('method'),
            url: yiiform.attr('action'),
            data: yiiform.serializeArray()
        }
    )
        .done(function(data) {
            if(data.success ) {
                // данные сохранены
                swal(orhpos_lang['m_title_success'],orhpos_lang['m_cont__success'], "success");
            } else {
                // сервер вернул ошибку и не сохранил наши данные
                swal({
                    title: orhpos_lang['m_title__warn'],
                    text: orhpos_lang['m_cont__warn'],
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: orhpos_lang['m_mod__but'],
                    closeOnConfirm: false
                })
            }
        })
        .fail(function () {
            // не удалось выполнить запрос к серверу
            alert(orhpos_lang['m_not_found']);
        })

    $('#orphos').modal('hide');

    return false; // отменяем отправку данных формы



});





