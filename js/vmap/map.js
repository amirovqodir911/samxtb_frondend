/**
 * Created by a_mirvosiqov on 24.11.2014.
 * Modified for Samarkand region by orzu@mail.ru on 30.01.2018
 */
$(document).ready(function () {
   /////////*****Map 1 territorial organisation ********////////////
    $('#vmap').css(
        {
            'width': '100%',
            'height': '360px'
        }
    );


    $('.mapPoints a').click(function(e){
        e.preventDefault();
        $('#mapLabels').toggleClass('open');
    });

    $('.mapPoints a').hover(function(){
        filterRegions($(this).attr('href'));
    }, function(){
        filterRegions('');
    });
    $(window).click(function(e){
        if(e.target.className.animVal != "jvectormap-region"){
            //console.log(e.target.className.animVal);
            $('#mapLabels').html('');
        }
    });
});

var regions = {
    qosh: 0,
    pay: 0,
    kat: 0,
    isht: 0,
    oqd: 0,
    jom: 0,
    bul: 0,
    toy: 0,
    urg: 0,
    sat: 0,
    pas: 0,
    nur: 0,
    pax: 0,
	nar: 0,
	ssh: 0,
	ksh: 0
	
};

function filterRegions(regionId) {
    var regionsList = {};
    $.each(regions, function (key, value) {
        var elementColor = '#4da9ec';
        regionsList[key] = elementColor;
    });
    if (regionId.length > 0){
        regionsList[regionId] = '#57bb64';
    }
    $('#vmap').vectorMap('set', 'colors', regionsList);
    return false;
}

//apiEvents.push({onMouseDown: "mouseDown",onMouseUp: "mouseUp"});
//console.log(apiEvents);


var coloreds = {
    qosh: '#5bc0de',
    pay: '#5bc0de',
    kat: '#5bc0de',
    isht: '#5bc0de',
    oqd: '#5bc0de',
    jom: '#5bc0de',
    bul: '#5bc0de',
    toy: '#5bc0de',
    urg: '#5bc0de',
    sat: '#5bc0de',
    pas: '#5bc0de',
    nur: '#5bc0de',
    pax: '#5bc0de',
	nar: '#5bc0de',
	ssh: '#5bc0de',
	ksh: '#5bc0de'
};

function makeMapEnd(messages) {
    $('#vmap').html('');
    $('.jqvmap-label').remove();
    $('#vmap').vectorMap({
        map: 'samarkand',
        backgroundColor: '',
        color: '#5bc0de',
        hoverColor: '#0091ff',
        selectedColor: '#0091ff',//цвет при нажатии
        enableZoom: false,
        showTooltip: true,
        borderColor: '#fff',
        borderWidth: 1,
        borderOpacity: 1,
        colors: coloreds,
        onRegionClick: function (event, label, code) {
            $('#mapLabels').html(messages[label]);
            //console.log(messages[label]);
        },
        onRegionSelect: function (event, label, code) {
            //$('#mapLabels').html(messages[label]);
            //console.log('2222');
            //console.log(label);
        },
        onRegionDeselect: function(){
            $('#mapLabels').html('');
            console.log('3333');
        },
        onLabelShow: function (event, label, code) {
            $('.jqvmap-label').html(messages[code]);
        }

    });


}


function makeMap(regions, messages, colors) {
    $('.mainMap').html('');
    $('.mapLabels2').remove();
    $('.mainMap').vectorMap({
        map: 'samarkand',
        backgroundColor: '',
        // color: '#49c7a2',
        hoverColor: '#c8c8c8',
        cursor: 'pointer',
        //selectedColor: '#337ab7',//цвет при нажатии
        enableZoom: false,
        showTooltip: true,
        borderColor: '#fff',
        borderWidth: 1,
        borderOpacity: 1,
        colors: icolors,
        onLabelShow: function (event, label, code) {
            $('.mapLabels2').html(messages[code]);
            $('.'+code).addClass('active');
        },
        onRegionOut: function(event, label, code){
            $('.mapLabels2').html('');


            // console.log($('#jqvmap1_' + label).attr('fills', '#fcffcc'));

            setTimeout(function(){
                $('#jqvmap1_' + label).attr('fill', icolors[label]);
            }, 100);
        },
        // onRegionClick: function (element, code, region) {
        //
        //     $('#map_modal_region .container_region_chart').attr('id','container_'+code);
        //     Highcharts.chart('container_'+code, {
        //         chart: {
        //             plotBackgroundColor: null,
        //             plotBorderWidth: null,
        //             plotShadow: false,
        //             type: 'pie'
        //         },
        //         title: {
        //             text: 'Browser market shares January, 2015 to May, 2015'
        //         },
        //         tooltip: {
        //             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        //         },
        //         legend: {
        //             enabled: true
        //         },
        //         plotOptions: {
        //             pie: {
        //                 allowPointSelect: true,
        //                 cursor: 'pointer',
        //                 dataLabels: {
        //                     enabled: true
        //                 },
        //                 showInLegend: true
        //             }
        //         },
        //         series: [{
        //             name: 'Brands',
        //             colorByPoint: true,
        //             data: [{
        //                 name: code,
        //                 y: 27
        //             }, {
        //                 name: 'Почтовые услуги',
        //                 y: 42,
        //                 sliced: true,
        //                 selected: true
        //             }, {
        //                 name: 'Икт, ТВ, образование',
        //                 y: 14
        //             }, {
        //                 name: 'Safari',
        //                 y: 13
        //             }, {
        //                 name: 'Chrome',
        //                 y: 4
        //             }]
        //         }]
        //     });
        //     $('#map_modal_region').modal('show');
        // }
    });
}



$('.vmapper').hover(function () {
        $(this).parents('body').toggleClass('mainMapBody');
        $('.jqvmap-label').css('display','none');
        $('.mapLabels2').css('display','none');
    });